var manifestData;
var serverUrl;
var doneOrgUnitSel = false;

$(document).ready(function() {
    loadManifest();
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
    if (window.File && window.FileReader) {
        $('#tableData').dataTable({
            'bFilter': false,
            'bSort': false
        });
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
});

/**
 * Loads file contents into dataTable
 * @param {type} evt
 * @returns {undefined}
 */
function handleFileSelect(evt) {
    $('#messages').removeClass('hidden');
    $('#tableData').dataTable().fnClearTable();

    var files = evt.target.files; // FileList object
    var csvFile = files[0]; // FileList object
    if (csvFile.type.match('application/vnd.ms-excel') || csvFile.type.match('text/csv')) {
        $('#messages').html(csvFile.name + ' - ' + csvFile.size + ' bytes, last modified: ' + csvFile.lastModifiedDate);
        var reader = new FileReader();
        reader.readAsText(csvFile);
        reader.onerror = function(e) {
            $('#messages').html('Error reading file');
        };
        reader.onload = function(e) {
            var readStr = e.target.result;
            readStr = readStr.split("\n").slice(1).join("\n");
            try {
                tableData = $.csv.toArrays(readStr);
                $('#tableData').dataTable().fnAddData(tableData);
                $('#btnImport').removeClass('hidden');
            } catch (err) {
                $('#messages').html('Error parsing: ' + err.message);
            }
        };
    } else {
        $('#messages').html('Incorrect file type');
    }
}

/**
 * Loads application manifest to scan for activities that map DHIS2 location
 * @returns {undefined}
 */
function loadManifest() {
    jQuery.getJSON('manifest.webapp').done(function(data) {
        manifestData = data;
        serverUrl = manifestData.activities.dhis.href;
        $('#btnExit').attr('href', serverUrl);
        getCurrentUser();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $.blockUI({message: 'Could not load manifest'});
    });
}

/**
 * Gets the current user to verify, if a user has logged in or not.
 * @returns {undefined} */
function getCurrentUser() {
    $.ajax({
        url: serverUrl + '/api/currentUser',
        headers: {
            'Accept': 'application/json'
        },
        type: "GET",
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    }).done(function(data, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('Login-Page') == 'true') {
            $.blockUI({message: $('#unauthenticatedMessage')});
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $.blockUI({message: $('#failureMessage')});
    });
}

function importUserAction() {
    corsSetup();

    clearOrgUnitSelectionUrl = serverUrl + '/dhis-web-commons/oust/clearSelectedOrganisationUnits.action';
    orgUnitSelectionUrl = serverUrl + '/dhis-web-commons/oust/addorgunit.action';

    oTable = $('#tableData').dataTable();
    tableNodes = oTable.fnGetNodes();

    var deferreds = [];
    tableNodes.each(function(index, tableRow){
        deferreds[index] = $.Deferred();
    });
    
    var defs = tableNodes.map(function(index, tableRow) {
        orgUnitStr = oTable.fnGetData(tableRow, 6);
        totalOU = orgUnitStr.split('|').length;
        return $.get(clearOrgUnitSelectionUrl);
    });

    $.when.apply(null, defs).then(function(data) {
        console.log('cleared, and orgUnitStr = ' + data);
    });

    $.ajaxSetup();


    /*
     * 
     OrgUnitSelector.clearOrgUnitSelection().then(function(data) {
     console.log('cleared, and orgUnitStr = ' + orgUnitStr);
     if (totalOU > 1) {
     $.each(orgUnitStr.split('|'), function(index, value) {
     console.log('INDEX = ' + index + " & VALUE = " + value);
     });
     }
     });
     * 
     * 
     * 
     * 
     * if (totalOU > 1) {    //Multiple OrgUnits
     $.each(orgUnitStr.split('|'), function(index, value) {
     $.when(postOrgUnitSelection('id=' + value, totalOU));
     });
     } else {    //Single OrgUnit
     postOrgUnitSelection('id=' + orgUnitStr, totalOU);
     }*/
}

OrgUnitSelector = {
    clearOrgUnitSelection: function() {
        var dfr = $.Deferred();
        clearOrgUnitSelectionUrl = serverUrl + '/dhis-web-commons/oust/clearSelectedOrganisationUnits.action';
        $.ajax({
            url: clearOrgUnitSelectionUrl,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            type: "GET",
            cache: false,
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            success: dfr.resolve
        });
        return dfr.promise();
    },
    postOrgUnitSelection: function() {
        var dfr = $.Deferred();
        orgUnitSelectionUrl = serverUrl + '/dhis-web-commons/oust/addorgunit.action';
        $.ajax({
            url: orgUnitSelectionUrl,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            type: "POST",
            cache: false,
            crossDomain: true,
            data: params,
            xhrFields: {
                withCredentials: true
            },
            success: dfr.resolve
        });
        return dfr.promise();
    }
};

function corsSetup() {
    $.ajaxSetup({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    });
}



function postUser() {
    $.ajax({
        type: "POST",
        url: serverUrl,
        data: data,
        success: success,
        dataType: dataType
    });
}

var createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        xhr.open(method, url, true);    // Most browsers.
    } else if (typeof XDomainRequest != "undefined") {
        xhr = new XDomainRequest();     // IE8 & IE9
        xhr.open(method, url);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
};